CPLEXUtils
==========

**CPLEXUtils** contains utility routines that allow a user to specify parameter settings for CPLEX and/or CPOptimizer at run time, using the Concert Java API, rather than having to specify those settings in the code.

With parameter setters from **CPLEXUtils** added to a program's code, a user can specify parameter names and values as strings, either as command line arguments or through a file or interactive interface. The setter then locates the named parameter, converts the value to the
correct type, and applies the parameter setting to the relevant solver. For example, passing the strings "MIP.Emphasis" and "2" to an instance of `CplexParameterSetter` would cause the search emphasis for the associated instance of `IloCplex` to switch from its default setting ("balanced") to "optimality".

The `CplexParameterSetter` class also provides a method for generating a text listing of all non-default parameter settings in an `IloCplex` object.

For more information and a usage example, please see the documentation for the cplexutils package.

If you just want a binary distribution (platform-agnostic .jar file and Javadoc folder), you can [download it](https://rubin.msu.domains/code/CPLEXUtils_dist.zip) from my university web site.

**CPLEXUtils** is written in Java 8.

Author: Paul A. Rubin [rubin@msu.edu](mailto:rubin@msu.edu)

Version: 2.1

License: Eclipse Public License 1.0
[http://www.eclipse.org/legal/epl-v10.html](http://www.eclipse.org/legal/epl-v10.html)

Other legal stuff: CPLEX, CPOptimizer and Concert are products of IBM Corporation.