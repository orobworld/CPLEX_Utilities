package cplexutils;

import ilog.concert.IloException;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.Param;
import ilog.cplex.IloCplex.BooleanParam;
import ilog.cplex.IloCplex.BooleanParameter;
import ilog.cplex.IloCplex.DoubleParam;
import ilog.cplex.IloCplex.DoubleParameter;
import ilog.cplex.IloCplex.IntParam;
import ilog.cplex.IloCplex.IntParameter;
import ilog.cplex.IloCplex.LongParam;
import ilog.cplex.IloCplex.LongParameter;
import static ilog.cplex.IloCplex.Parameter.BooleanType;
import static ilog.cplex.IloCplex.Parameter.DoubleType;
import static ilog.cplex.IloCplex.Parameter.IntType;
import static ilog.cplex.IloCplex.Parameter.LongType;
import static ilog.cplex.IloCplex.Parameter.StringType;
import static ilog.cplex.IloCplex.Parameter.UnknownType;
import ilog.cplex.IloCplex.StringParam;
import ilog.cplex.IloCplex.StringParameter;
import java.lang.reflect.Field;
import java.util.Iterator;

/**
 * CplexParameterSetter provides a mechanism for specifying CPLEX parameters
 * by name in the command line of a Java program, as well as a method for
 * listing non-default parameter settings in an instance of IloCplex.
 *
 * <p>Example: Suppose that a program is called with</p>
 * <pre>
 *    java -jar myprog.jar TimeLimit 30 MIP.Display 100
 * </pre><p>Within the main program,</p>
 * <pre>
 *   IloCplex c;
 *   CplexParameterSetter setter = new CplexParameterSetter();
 *   setter.set(c, args[0], args[1]);
 *   setter.set(c, args[2], args[3]);
 * </pre><p>(within a try-catch block) creates a CPLEX instance and sets
 * the time limit parameter to 30 seconds and the MIP log display interval
 * to 100.</p>
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 * @version 2.1
 *
 * Licensed under the Eclipse Public License 1.0
 * (http://www.eclipse.org/legal/epl-v10.html).
 */
public final class CplexParameterSetter {
  private final OptionsCatalog catalog;  // catalog of known options

  /**
   * Constructor.
   */
  public CplexParameterSetter() {
    catalog = new OptionsCatalog(OptionsCatalog.Solver.CPLEX);
  }

  /**
   * Sets the value of a CPLEX parameter.
   * @param cplex the solver instance in which the parameter will be set
   * @param name the (case-sensitive) Java symbolic name of the parameter
   * @param value a string containing the new value for the parameter
   * @throws UnknownParameterException if the parameter name is not recognized
   * @throws NullArgumentException if a null or empty argument is passed
   * @throws IloException if CPLEX balks at the parameter value
   * @throws IllegalAccessException (should never happen)
   * @throws AmbiguousParameterException if the parameter name is ambiguous
   */
  public final void set(final IloCplex cplex,
                        final String name,
                        final String value)
                    throws UnknownParameterException,
                           NullArgumentException,
                           IloException,
                           IllegalAccessException,
                           AmbiguousParameterException {
    // Check for invalid inputs.
    if (cplex == null || name == null || name.isEmpty()
        || value == null || value.isEmpty()) {
      throw new NullArgumentException();
    }
    // Get the named field.
    Field f = catalog.getField(name);
    Object param = f.get(cplex);
    // Get the type of the field.
    Class t = catalog.fieldType(f);
    // Try to set the parameter.
    if (t == int.class) {
      int v = Integer.parseInt(value);
      cplex.setParam((IntParam) param, v);
    } else if (t == long.class) {
      long v = Long.parseLong(value);
      cplex.setParam((LongParam) param, v);
    } else if (t == double.class) {
      double v = Double.parseDouble(value);
      cplex.setParam((DoubleParam) param , v);
    } else if (t == boolean.class) {
      boolean v = Boolean.parseBoolean(value);
      cplex.setParam((BooleanParam) param, v);
    } else if (t == String.class) {
      cplex.setParam((StringParam) param, value);
    }
  }

  /**
   * Generates a string summary of all non-default parameter settings for
   * an instance of IloCplex.
   * @param cplex the IloCplex instance
   * @return a summary of non-default parameters (names and values)
   * @throws IloException if CPLEX balks at scanning the parameter set
   */
  public String summarizeParameterSet(final IloCplex cplex)
                throws IloException {
    StringBuilder sb = new StringBuilder();
    // Get an iterator over the CPLEX object's parameter set.
    Iterator<Param> it = cplex.getParameterSet().iterator();
    while (it.hasNext()) {
      // Get the next parameter.
      Object x = it.next();
      IloCplex.Parameter y = (IloCplex.Parameter) x;
      String key;
      // Match it to its type and get its key (as a string) and value.
      // Suppress the output if the key is "HIDDEN".
      switch (y.getType()) {
        case BooleanType:
          BooleanParameter b = (BooleanParameter) y;
          key = b.getKey().toString();
          if (!"HIDDEN".equals(key)) {
            sb.append(key).append(" = ").append(b.getValue()).append("\n");
          }
          break;
        case DoubleType:
          DoubleParameter d = (DoubleParameter) y;
          key = d.getKey().toString();
          if (!"HIDDEN".equals(key)) {
            sb.append(key).append(" = ").append(d.getValue()).append("\n");
          }
          break;
        case IntType:
          IntParameter i = (IntParameter) y;
          key = i.getKey().toString();
          if (!"HIDDEN".equals(key)) {
            sb.append(key).append(" = ").append(i.getValue()).append("\n");
          }
          break;
        case LongType:
          LongParameter l = (LongParameter) y;
          key = l.getKey().toString();
          if (!"HIDDEN".equals(key)) {
            sb.append(key).append(" = ").append(l.getValue()).append("\n");
          }
          break;
        case StringType:
          StringParameter s = (StringParameter) y;
          key = s.getKey().toString();
          if (!"HIDDEN".equals(key)) {
            sb.append(key).append(" = ").append(s.getValue()).append("\n");
          }
          break;
        case UnknownType:
          sb.append("Encountered unknown parameter type ")
            .append(y.getType()).append("\n");
          break;
        default:
      }
    }
    return sb.toString();
  }
}