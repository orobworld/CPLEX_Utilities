package cplexutils;

import ilog.concert.IloException;
import ilog.cplex.IloCplex;

/**
 * HiddenParameter allows a user to access "hidden" CPLEX parameters (provided
 * the user knows the integer identifier and type of the parameter).
 *
 * For example, if the user has an IloCplex instance named "mip" and wishes
 * to set hidden parameter 2201 to the value -1, the following line would
 * be added to the user's code:
 *   HiddenParameter.setParam(mip, HiddenParameter.Type.INT, 2201, -1,
 *                            "Undocumented _precedence parameter");
 *
 * This feature is highly experimental.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class HiddenParameter {
  /** Dummy constructor to prevent users instantiating the class. */
  private HiddenParameter() { }

  /**
   * Sets a value for a hidden CPLEX parameter.
   * @param cplex the IloCplex instance on which the parameter is set
   * @param type the type of parameter
   * @param id the integer ID of the hidden parameter
   * @param value the value to be set for the parameter (must be coercible to
   * the class indicated by the `type` argument
   * @param label a user-supplied label for the hidden parameter
   * @throws IloException if CPLEX cannot set the parameter
   */
  public static void setParam(final IloCplex cplex, final Type type,
                              final int id, final Object value,
                              final String label)
                     throws IloException {
    switch (type) {
      case BOOLEAN:
        BooleanParam pb = new BooleanParam(id, label);
        cplex.setParam(pb, (boolean) value);
        break;
      case DOUBLE:
        DoubleParam pd = new DoubleParam(id, label);
        cplex.setParam(pd, (double) value);
        break;
      case INT:
        IntParam pi = new IntParam(id, label);
        cplex.setParam(pi, (int) value);
        break;
      case LONG:
        LongParam pl = new LongParam(id, label);
        cplex.setParam(pl, (long) value);
        break;
      case STRING:
        StringParam ps = new StringParam(id, label);
        cplex.setParam(ps, (String) value);
        break;
      default:
        throw new IllegalArgumentException("Invalid hidden parameter type "
                                           + type + ".");
    }
  }

  /**
   * Type enumerates the CPLEX parameter types.
   */
  public enum Type {
    /** BOOLEAN means IloCplex.BooleanParam. */
    BOOLEAN,
    /** DOUBLE means IloCplex.DoubleParam. */
    DOUBLE,
    /** INT means IloCplex.IntParam. */
    INT,
    /** LONG means IloCplex.LongParam. */
    LONG,
    /** STRING means IloCplex.StringParam. */
    STRING
  }

  /**
   * BooleanParam produces a hidden boolean parameter.
   */
  private static class BooleanParam extends IloCplex.BooleanParam {
    private static final long serialVersionUID = 1L;

    /**
     * Constructor.
     * @param id the integer identifier of the hidden parameter
     * @param label a label for the parameter
     */
    BooleanParam(final int id, final String label) {
      super(id, label);
    }
  }

  /**
   * DoubleParam produces a hidden double parameter.
   */
  private static class DoubleParam extends IloCplex.DoubleParam {
    private static final long serialVersionUID = 2L;

    /**
     * Constructor.
     * @param id the integer identifier of the hidden parameter
     * @param label a label for the parameter
     */
    DoubleParam(final int id, final String label) {
      super(id, label);
    }
  }

  /**
   * IntParam produces a hidden integer parameter.
   */
  private static class IntParam extends IloCplex.IntParam {
    private static final long serialVersionUID = 3L;

    /**
     * Constructor.
     * @param id the integer identifier of the hidden parameter
     * @param label a label for the parameter
     */
    IntParam(final int id, final String label) {
      super(id, label);
    }
  }

  /**
   * LongParam produces a hidden long parameter.
   */
  private static class LongParam extends IloCplex.LongParam {
    private static final long serialVersionUID = 4L;

    /**
     * Constructor.
     * @param id the integer identifier of the hidden parameter
     * @param label a label for the parameter
     */
    LongParam(final int id, final String label) {
      super(id, label);
    }
  }

  /**
   * StringParam produces a hidden string parameter.
   */
  private static class StringParam extends IloCplex.StringParam {
    private static final long serialVersionUID = 5L;

    /**
     * Constructor.
     * @param id the integer identifier of the hidden parameter
     * @param label a label for the parameter
     */
    StringParam(final int id, final String label) {
      super(id, label);
    }
  }
}
